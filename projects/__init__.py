from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
app = Flask(__name__)

# database connection

app.config['SECRET_KEY'] = 'wow-s3cr3t-k3Y-woW-!!1!!1'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/flask'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)
from projects.models import user
from projects.models import todo

from projects.routes import routes

if __name__ == '__main__':
    app.run(debug=True)