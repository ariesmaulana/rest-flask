from datetime import datetime
from projects import app, db
from sqlalchemy.orm import validates

class Todo(db.Model):
    __tablename__='todo'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    status =  db.Column(db.Boolean, default=False)
    user_id =  db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False, default=0)
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    
    def __repr__(self):
        return "Todo {}".format(self.title)

    @validates('title')
    def validate_title(self, key, title):
        if len(title) < 5 or len(title) > 20:
            raise AssertionError('Title must be between 5 and 20 characters')

        return title