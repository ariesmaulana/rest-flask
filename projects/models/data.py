def Todos():
    todos = [
        {
            'id': 1,
            'title':'Nafas',
            'status': True,
        },
        {
            'id': 2,
            'title':'Makan',
            'status': True,
        },
        {
            'id': 3,
            'title':'Minum',
            'status': True,
        },
        {
            'id': 4,
            'title':'Tidur',
            'status': False,
        }
    ]
    return todos