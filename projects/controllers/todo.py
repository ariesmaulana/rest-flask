from projects import app
from projects import db
from flask import Blueprint,  request, json, Response, jsonify
from projects.models.todo import Todo
from projects.helpers.helper import token_required
todo  = Blueprint("todo",__name__)


# route untuk get todo, defaultnnya todo yang belum dikerjakan
# dan post untuk tambah baru
@todo.route('/todo', methods=['GET', 'POST'])
@token_required
def index(current_user):
    if request.method == 'POST':
        try: 
            new_todo = Todo(title=request.form['title'],user_id=current_user.id)
            db.session.add(new_todo)
            db.session.commit()
            return jsonify({'message' : 'Data telah ditambahkan'})
        except AssertionError as exception_message:
             return jsonify(msg='Error: {}. '.format(exception_message)), 400
   
    else:
        
        page = request.args.get("page", 1, type=int)
        status = request.args.get("status", 'pending', type=str)
        status = True if status == 'finish' else False
        limit = 5
        lists = Todo.query.filter_by(status=status,user_id=current_user.id).paginate(page, limit, False)
        output = []
        
        for data in lists.items:
            todo_list = {}
            todo_list['id'] = data.id
            todo_list['title'] = data.title
            todo_list['status'] = data.status
            output.append(todo_list)

        prev_page = lists.prev_num if lists.has_prev else None
        next_page = lists.next_num if lists.has_next else None
        page = {
            'prev_page' : prev_page,
            'next_page': next_page
        }
        return jsonify({'list' : output, 'page' : page })


# route get with parameter
@todo.route('/todo/<int:id>', methods=['GET'])
def detail_todo(id):

    todolist = {}
    data = Todo.query.filter_by(id=id).first()
    if not data:
        return jsonify({'data' : todolist })
    todolist['id'] = data.id
    todolist['title'] = data.title
    todolist['status'] = data.status
    return jsonify({'data' : todolist })

@todo.route('/todo/<int:id>/update', methods=['PUT'])
def update_todo(id):
    data = Todo.query.filter_by(id=id).first()
    try:
        data.status = True
        db.session.commit()
        return jsonify({'message' : 'Data telah selesai dikerjakan' })
    except:
        return jsonify({'msg' : 'Terjadi kesalahan dalam hubungan ke database, segera hubungi admin.' }), 500
@todo.route('/todo/<int:id>/delete', methods=['DELETE'])
def delete_tode(id):
    data = Todo.query.filter_by(id=id).first()
    try:
        db.session.delete(data)
        db.session.commit()
        return jsonify({'message' : 'Data telah dihapus' })
    except:
        return jsonify({'msg' : 'Terjadi kesalahan dalam hubungan ke database, segera hubungi admin.' }), 500
