from projects import app
from flask import Blueprint,  request, json, Response

home  = Blueprint("home",__name__)

# Home Route
@home.route('/')
def index():
    data = {
        "data": "selamat datang di flask"
    }
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp