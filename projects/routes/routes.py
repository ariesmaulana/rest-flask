from projects import app

from projects.controllers.home import home
from projects.controllers.todo import todo
from projects.controllers.user import user

app.register_blueprint(home)
app.register_blueprint(todo)
app.register_blueprint(user)